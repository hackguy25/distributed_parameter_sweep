#!/usr/bin/env python3

import sys

# just some meaningless computation, to have the CPU do something for a second
if __name__ == "__main__":
    a = 3.4
    b = float(sys.argv[1])
    for i in range(10000000):
        if a < 5:
            a = 3 ** (a + 4)
        else:
            a = a ** b
    print(a)