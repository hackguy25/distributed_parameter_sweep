#!/usr/bin/env python3

import sys

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Got nothing!")
    else:
        filler = "', '"
        print(f"Got {len(sys.argv) - 1} argument(s): ['{filler.join(sys.argv[1:])}']")