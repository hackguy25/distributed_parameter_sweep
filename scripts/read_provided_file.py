#!/usr/bin/env python3

import sys

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Got nothing!")
    else:
        try:
            with open(sys.argv[1]) as f_in:
                contents = f_in.read()
            print(f"Got {contents} ({sys.argv[1]})")
        except:
            print(f"Couldn't read file! ({sys.argv[1]})")