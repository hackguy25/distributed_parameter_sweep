#!/usr/bin/env python3

import sys, json, itertools
from os.path import dirname, basename, join as join_path
from os import system
from copy import deepcopy
from pathlib import Path

"""
A module.

Used to analyse a set of experiments performed by a parameter sweep.

Example usage: 
import pism_parameter_sweep_analysis
sg = SweepGenerator(host_fname, sim_fname, param_sweep_fname)
"""

class SweepGenerator:
  def __init__(self, host_json, sim_json, param_json) -> None:
    self.initFromFiles(host_json, sim_json, param_json)

  def initFromFiles(self, host_json, sim_json, param_json):
    # check if the files exist 
    if not Path(host_json).exists():
      raise RuntimeError(f"File {host_json} not found.")
    if not Path(sim_json).exists():
      raise RuntimeError(f"File {sim_json} not found.")
    if not Path(param_json).exists():
      raise RuntimeError(f"File {param_json} not found.")

    # load files
    try:
      with open(host_json) as f_in:
        self.host = json.load(f_in)
      with open(sim_json) as f_in:
        self.defaults = json.load(f_in)
      with open(param_json) as f_in:
        self.params = json.load(f_in)
    except Exception as e:
      raise RuntimeError(f"Failed to parse file as json: {str(e)}")

    self.prepareSweepValues()

  def prepareSweepValues(self):
    self.param_names = []
    self.param_values = []
    for param in self.params:
      self.param_names.append(param[0])
      if param[1] == "multiply":
        param_base = self.defaults[param[0]]
        self.param_values.append([param_base * i for i in param[2]])
      elif param[1] == "add":
        param_base = self.defaults[param[0]]
        self.param_values.append([param_base + i for i in param[2]])
      else:
        self.param_values.append(param[1][:])

    # iterate over all parameter combinations
    self.experiment_files = []
    self.experiment_values = []
    for vals in itertools.product(*self.param_values):
      # find the directory of the experiment
      experiment_name = "_".join(self.param_names[i].replace(" ", "_") + "_" + str(vals[i]) for i in range(len(vals)))
      # TODO: replace the one below with Path(self.defaults["output fname"]).parent; ditto for all paths
      main_dir = dirname(self.defaults["output fname"])
      experiment_dir = join_path(main_dir, experiment_name)

      # create host and experiment JSON files
      host_fname = join_path(experiment_dir, "host.json")
      experiment_fname = join_path(experiment_dir, "experiment.json")
      self.experiment_files.append((Path(experiment_dir), Path(host_fname), Path(experiment_fname),))
      self.experiment_values.append({self.param_names[i]:vals[i] for i in range(len(vals))})

  def print(self):
    """Print everything about the instance to the console.
    
    This function also demonstrates how the list variables are interconnected.
    """
    print("inputs:")
    for i in range(len(self.param_names)):
      print(f"- {self.param_names[i]:20s}: {self.param_values[i]}")
    print("Experiments:")
    for i in range(len(self.experiment_values)):
      print(f"{i:3d}: {self.experiment_values[i]}, {self.experiment_files[i]}")
    pass


if __name__ == "__main__":
  # process inputs
  if len(sys.argv) > 3:
    host_json = sys.argv[1]
    sim_json = sys.argv[2]
    param_json = sys.argv[3]
  elif len(sys.argv) == 1:
    host_json, sim_json, param_json = "json/host_defaults.json", "json/sn_mipro2022_baseline.json", "json/sweep_example.json"
  else:
    print(f"error, {sys.argv[0]} requires 3 input files passed as arguments")
    exit(1)
  sg = SweepGenerator(host_json, sim_json, param_json)
  print("everything ok")
  sg.print()
