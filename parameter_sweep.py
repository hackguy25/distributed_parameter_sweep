#!/usr/bin/env python3

import argparse
import tempfile
import json
import os
import itertools
from collections import deque
from multiprocessing import Pool
from pathlib import Path


class SweepGenerator:
    """
    Generates all combinations of parameters.

    To construct a SweepGenerator, provide a list of default values and a list of parameters to vary.
    Each list can be given either as an object (using 'defaults' and 'params' arguments) or as a
    path to a JSON file (using 'defaults_file' and 'params_file' arguments).
    A SweepGenerator object can then be used to construct iterators over combinations of parameter
    values.
    """

    def __init__(self, *, defaults=None, defaults_file=None, params=None, params_file=None):
        # parse default values
        if defaults:
            # default values are given either as a JSON string or a dict
            if type(defaults) == str:
                try:
                    self.defaults = json.loads(defaults)
                except Exception as e:
                    raise RuntimeError(
                        f"failed to parse string as json: {str(e)}")
            elif type(defaults) == dict:
                self.defaults = defaults
            else:
                raise RuntimeError(
                    "default values must be given as a dict or a JSON string")
        elif defaults_file:
            # default values are given in a JSON file
            try:
                with open(defaults_file) as f_in:
                    self.defaults = json.load(f_in)
            except Exception as e:
                raise RuntimeError(f"failed to parse file as json: {str(e)}")
        else:
            raise RuntimeError("neither 'defaults' nor 'defaults_file' set")

        # parse parameters to vary
        if params:
            # parameter values are given either as a JSON string or a list
            if type(params) == str:
                try:
                    self.params = json.loads(params)
                except Exception as e:
                    raise RuntimeError(
                        f"failed to parse string as json: {str(e)}")
            elif type(params) == list:
                self.params = params
            else:
                raise RuntimeError(
                    "parameter values must be given as a dict or a JSON string")
        elif params_file:
            # parameter values are given in a JSON file
            try:
                with open(params_file) as f_in:
                    self.params = json.load(f_in)
            except Exception as e:
                raise RuntimeError(f"failed to parse file as json: {str(e)}")
        else:
            raise RuntimeError("neither 'params' nor 'params_file' set")

        # parse and prepare parameters
        self.param_names = []
        self.param_values = []
        self.param_set = set()
        for param in self.params:
            # parse parameter name, type and value
            try:
                param_name = param[0]
                param_type = "literal"
                if len(param) == 2:
                    param_values = param[1][:]
                elif len(param) == 3:
                    param_type = param[1]
                    if param_type not in ["add", "multiply"]:
                        raise RuntimeError(
                            f"invalid parameter type: '{param_type}'")
                    param_values = param[2][:]
                else:
                    raise RuntimeError(
                        f"parameter specification contains {len(param)} elements")
            except Exception as e:
                raise RuntimeError(
                    f"parameters must have one of the following forms: "
                    "[name, [values]], [name, 'add', [values]] or "
                    "[name, 'multiply', [values]], got {param}, failed with: {e}")

            # check if parameter was already given
            if param_name in self.param_set:
                raise RuntimeError(
                    f"parameter '{param_name}' was given twice")

            # save the parameter
            self.param_names.append(param_name)
            self.param_set.add(param_name)
            if param_type == "multiply":
                param_base = self.defaults.get(param_name)
                if param_base == None:
                    raise RuntimeError(
                        "option 'multiply' requires a default value to be set")
                self.param_values.append(
                    [param_base * i for i in param_values])
            elif param_type == "add":
                param_base = self.defaults.get(param_name)
                if param_base == None:
                    raise RuntimeError(
                        "option 'add' requires a default value to be set")
                self.param_values.append(
                    [param_base + i for i in param_values])
            else:
                self.param_values.append(param_values)

    def iterate(self, output=None, permissive=False):
        """
        Creates a generator over parameter combinations which generates dicts of parameters.

        If provided, the 'output' argument should be a path to an existing directory.
        If an output directory is provided, the returned dict contains an additional value
        'output', which contains a path to a subdirectory for the corresponding parameter set.
        """
        if output:
            # prepare output folder
            out_path = Path(output)
            if not out_path.is_dir():
                raise RuntimeError("given output path is not a directory")

            # iterate over all parameter combinations
            for vals in itertools.product(*self.param_values):
                # assemble experiment name
                experiment_name = "_".join(
                    self.param_names[i] +
                    "_" + str(vals[i])
                    for i in range(len(vals))
                ).replace(" ", "_")
                # create experiment directory
                experiment_dir = Path(out_path, experiment_name)
                if experiment_dir.exists():
                    try:
                        os.rmdir(experiment_dir)
                        # there existed an empty directory
                        os.mkdir(experiment_dir)
                    except OSError:
                        # non-empty directory exists
                        if permissive:
                            # skip this parameter set
                            # TODO: notify to stdout?
                            continue
                        else:
                            raise RuntimeError(
                                f"existing data found in {experiment_dir}, "
                                "clean up the output directory or consider "
                                "using permissive folder generation"
                            )
                else:
                    os.mkdir(experiment_dir)

                yield self.defaults | dict(zip(self.param_names, vals)) | {"output": experiment_dir}
        else:
            # iterate over all parameter combinations
            for (i, vals) in enumerate(itertools.product(*self.param_values)):
                yield self.defaults | dict(zip(self.param_names, vals))


def sweep_sequential_local(
    command, sg, *,
    to_format=False, output=None, permissive=False
):
    """
    Executes a sequential sweep of a given command with a given SweepGenerator.

    If the output folder is provided, then subfolders are generated and included
    in the parameter dicts.
    If 'to_format' is set to True, then the command is treated as a format string
    and the dict is used to format it.
    """
    for params in sg.iterate(output, permissive=permissive):
        if "output" in params:
            params["output"] = str(params["output"])
        if to_format:
            os.system(command.format(**params))
        else:
            os.system(f"{command} '{json.dumps(params)}'")


def sweep_parallel_local(
    command, sg, process_num, *,
    to_format=False, output=None, permissive=False
):
    """
    Executes a parallel sweep of a given command with a given SweepGenerator.

    If the output folder is provided, then subfolders are generated and included
    in the parameter dicts.
    If 'to_format' is set to True, then the command is treated as a format string
    and the dict is used to format it.
    """
    def generate_execs(command, params_iter, format):
        for params in sg.iterate(output, permissive=permissive):
            if "output" in params:
                params["output"] = str(params["output"])
            if to_format:
                yield command.format(**params)
            else:
                yield f"{command} '{json.dumps(params)}'"

    with Pool(process_num) as p:
        # consume iterator efficiently
        # (see https://docs.python.org/3/library/itertools.html#itertools-recipes , consume)
        # TODO: set chunksize?
        deque(
            p.imap_unordered(
                os.system,
                generate_execs(command, sg.iterate(output), to_format)
            ),
            maxlen=0
        )


# TODO: deprecated for now, needs to be discussed
# def sweep_parallel_kocka(function, defaults, to_sweep, kocke):
#     params_to_sweep = list(to_sweep.keys())
#     combinations = itertools.product(*(to_sweep[i] for i in params_to_sweep))

#     with tempfile.TemporaryDirectory(dir=".") as temp_dir:
#         with open(os.path.join(temp_dir, f"execs"), "w") as execs:
#             for (i, vals) in enumerate(combinations):
#                 for j in range(len(params_to_sweep)):
#                     defaults[params_to_sweep[j]] = vals[j]
#                 temp_f_path = os.path.join(temp_dir, f"temp_{i}.json")
#                 with open(temp_f_path, "w") as temp_f:
#                     json.dump(defaults, temp_f)
#                 execs.write(os.path.realpath(function) + " " +
#                             os.path.realpath(temp_f_path) + "\n")

#         command = ("kocka-forcerun -i " +
#                    os.path.realpath(os.path.join(temp_dir, f"execs")) +
#                    " -o " +
#                    os.path.realpath("./kocka_results") +
#                    " -k \"" + kocke + "\" -p 1")
#         os.system(command)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Perform a parameter sweep.")
    parser.add_argument(
        "function",
        metavar="COMMAND",
        help="command to execute"
    )
    parser.add_argument(
        "-df",
        "--defaults-file",
        metavar="PATH",
        help="path to JSON file containing the default parameter values"
    )
    parser.add_argument(
        "-d",
        "--defaults",
        metavar="JSON",
        help="JSON string containing the default parameter values, "
        "e.g. '{\"foo\": \"a\", \"bar\": 0.2, \"baz\": 3}' "
        "(important: single quotes around the outer string, double quotes around inner strings)"
    )
    parser.add_argument(
        "-pf",
        "--params-file",
        metavar="PATH",
        help="path to JSON file containing parameters to sweep over"
    )
    parser.add_argument(
        "-p",
        "--params",
        metavar="PATH",
        help="JSON string containing parameters to sweep over, "
        "e.g. '[[\"foo\", [\"a\", \"b\", \"c\"]], [\"bar\", \"add\", [0.1, 0.3, 0.5]]]' "
        "(important: single quotes around the outer string, double quotes around inner strings)"
    )
    parser.add_argument(
        "-t",
        "--distributed",
        metavar="N",
        type=int,
        help="enable distributed sweep with N processes"
    )
    # TODO: deprecated for now
    # parser.add_argument(
    #     "-k",
    #     "--kocka",
    #     metavar="K",
    #     nargs='+',
    #     help="enable sweep on kocka (must be executed on ninestein), providing a list of "
    #     "valid kocka hostnames, e.g. k17 k18"
    # )
    parser.add_argument(
        "-o",
        "--output-dir",
        metavar="PATH",
        help="path to an empty directory where the results should be stored"
    )
    parser.add_argument(
        "-f",
        "--format-command",
        action="store_true",
        help="treat the given command as a Python format string"
    )
    parser.add_argument(
        "-g",
        "--permissive-folder-generation",
        action="store_true",
        help="when generating output subfolders, instead of aborting "
        "check if an existing subfolder is populated, "
        "and skip the corresponding parameter set"
    )

    args = parser.parse_args()
    sg = SweepGenerator(
        defaults=args.defaults,
        defaults_file=args.defaults_file,
        params=args.params,
        params_file=args.params_file,
    )

    if args.distributed:
        # if args.kocka:
        #     print("Cannot use -d and -k simultaneously!")
        #     exit(1)
        sweep_parallel_local(
            args.function, sg, args.distributed,
            output=args.output_dir, to_format=args.format_command,
            permissive=args.permissive_folder_generation
        )
    # elif args.kocka:
    #     print("Running on kocka is currently not implemented.")
        # sweep_parallel_kocka(args.function, defaults,
        #                      to_sweep, " ".join(args.kocka))
    else:
        sweep_sequential_local(
            args.function, sg,
            output=args.output_dir, to_format=args.format_command,
            permissive=args.permissive_folder_generation
        )
