# Distributed parameter sweep

A script which performs a parameter sweep over given parameters.

The script expects either a JSON string or a path to a JSON file, for default values and for variable parameters.
The default values should be provided as a valid JSON object (dictionary).
The variable parameters should be provided as a valid JSON array (list), where each element of this array is an array containing 2 or 3 elements (see examples).

Path to an empty output directory can be provided, in which case the script creates a subfolder for each set of parameter values and includes its path in the parameter set (see examples).
If the output directory is not specified, nothing is created (useful when the command creates its own file structure already).

If the distributed sweep is selected, then the command is executed using a pool of N processes.

The given command can be treated as a Python format string.
In this case the parameters are substituted into the format string before execution.
Otherwise, the parameter set is provided to the command as a JSON string command line argument.

When an output directory is given, the subfolder generation can be either strict (default) or permissive.
Strict behavior stops iteration as soon as a non-empty subdirectory is found.
Permissive behavior skips all parameter sets for which a non-empty subdirectory is found.
Empty subdirectories are considered nonexistent.
An example use-case for permissive behavior is a second sweep with the same parameters as the first because the first sweep failed midway due to an external error.

## Examples:

- Perform a sequential parameter sweep, parameters are given to the command as a JSON string: `./parameter_sweep.py -d '{"foo": "a", "bar": 0.2, "baz": 3}' -p '[["foo", ["a", "b", "c", "d", "e"]], ["bar", "add", [0.1,0.3,0.5]]]' scripts/print_arguments.py`
- Perform a parallel parameter sweep: `./parameter_sweep.py -d '{"foo": "a", "bar": 0.2, "baz": 3}' -p '[["foo", ["a", "b", "c", "d", "e"]], ["bar", "add", [0.1,0.3,0.5]]]' -t 4 scripts/print_arguments.py`
- Perform a sequential parameter sweep, reading parameter values from a file: `./parameter_sweep.py -df data/sample_defaults.json -pf data/sample_params.json scripts/print_arguments.py`
- Perform a sequential parameter sweep with string formatting: `./parameter_sweep.py -d '{"foo": "a", "bar": 0.2, "baz": 3}' -p '[["foo", ["a", "b", "c", "d", "e"]], ["bar", "add", [0.1,0.3,0.5]]]' -f "scripts/print_arguments.py -b {baz} {foo}, {bar}"`
- Perform a parallel parameter sweep with string formatting, saving results in an output directory with permissive folder generation: `./parameter_sweep.py -d '{"foo": "a", "bar": 0.2, "baz": 3}' -p '[["foo", ["a", "b", "c", "d", "e"]], ["bar", "add", [0.1,0.3,0.5]]]' -o 'tmp' -t 12 -f -g "scripts/complex_task.py {bar} > {output}/stdout.txt"`


## TODO

- Reimplement functions to run on Kocka
- Try and test code with real world use
- Add parameter set filtering (e.g. "a > 0 or b > 0")